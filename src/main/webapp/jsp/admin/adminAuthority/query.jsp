<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

	<div id="query" style="background:#fff;">

		<table>
			<tr>
				<td>角色名称：</td>
				<td>
					<input type="text" id="roleNameQuery" placeholder="请输入角色名称">
				</td>
			</tr>
			<tr>
				<td>
					<button class="btn btn-default btn-xs" href="javascript:void(0)" onclick="doQuery()">查询</button>
				</td>
				<td></td>
			</tr>
		</table>
	</div>

	<script type="text/javascript">

		function doQuery() {

			var queryObject = new Object();
			queryObject.roleName = $("#roleNameQuery").val();

			$("#data").datagrid('load',queryObject);	
		}

	</script>
