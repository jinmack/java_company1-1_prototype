<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

	<div id="query" style="background:#fff;">

		<table>
			<tr>
				<td>付款单位（人）：</td>
				<td>
					<input type="text" id="userPaymentUnit" placeholder="请输入付款单位（人）">
				</td>
				<td>收款人姓名：</td>
				<td>
					<input type="text" id="emailPayeeName" placeholder="请输入收款人姓名">
				</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<button class="btn btn-default btn-xs" href="javascript:void(0)" onclick="doQuery()">查询</button>
				</td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>

	<script type="text/javascript">

		function doQuery() {

			var queryObject = new Object();
			queryObject.paymentUnit = $("#userPaymentUnit").val();
			queryObject.payeeName = $("#emailPayeeName").val();

			$("#data").datagrid('load',queryObject);	
		}

	</script>
