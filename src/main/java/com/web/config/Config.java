package com.web.config;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.template.Engine;
import com.web.remote.dao._MappingKit;


public class Config extends JFinalConfig {

	// 配置常量
	public void configConstant(Constants me) {
		
		// 开启调试模式
		me.setDevMode(true);
		
		// 加载少量必要配置，随后可用PropKit.get(...)获取值
		// 本地数据库
		PropKit.use("MysqlConfigLocal.txt");
		// 远程数据库
		// PropKit.use("MysqlConfigRemote.txt");
		me.setDevMode(PropKit.getBoolean("devMode", false));
		me.setError404View("/jsp/error/404.jsp");
		me.setError500View("/jsp/error/500.jsp");
	}

	// 配置路由
	public void configRoute(Routes me) {
		
		me.add(new AdminRoutesConfig()); // 后端路由
		me.add(new ServiceRoutesConfig()); // 工具路由
	}

	public void configEngine(Engine me) {

	}

	// 配置插件
	public void configPlugin(Plugins me) {
		
		// 配置C3p0数据库连接池插件
		DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
		me.add(druidPlugin);
		
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		
		// 所有映射在 MappingKit 中自动化搞定
		_MappingKit.mapping(arp);
		me.add(arp);
		
		// 配置EhCachePlugin缓存
		me.add(new EhCachePlugin());
	}

	// 配置全局拦截器
	public void configInterceptor(Interceptors me) {
		
	}

	// 配置处理器
	public void configHandler(Handlers me) {

	}

}
