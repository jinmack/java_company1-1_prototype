<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>企业管理系统</title>

	<link rel="stylesheet" type="text/css" href="/common/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="/common/easyui/themes/icon.css">
	<script type="text/javascript" src="/common/js/jquery.min.js"></script>
	<script type="text/javascript" src="/common/js/jquery.cookie.js"></script>
	<script type="text/javascript" src="/common/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/common/js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="/common/js/tool/operationCommon.js"></script>
	<script type="text/javascript" src="/common/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" src="/common/ueditor/ueditor.all.js"></script>
	<link rel="stylesheet" type="text/css" href="/common/bootstrap3/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/common/common.css">
</head>

<body class="easyui-layout">
