package com.web.action.admin;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Page;
import com.web.remote.dao.AdminAuthority;

public class AdminAdminAuthorityAction extends AdminBaseAction {
	
	public void index() {

		// renderText("/user/index");
		// System.out.println("index");
		// render("index.html");
		// renderNull();
		renderError(404);
	}
	
	// 权限页面显示
	public void adminAuthority(){
		
		renderJsp("/jsp/admin/adminAuthority/adminAuthority.jsp");
	}
	
	// 权限数据查询
	public void adminAuthorityJsonSeleft(){
		
		int page = getParaToInt("page", 1);
		int rows = getParaToInt("rows", 10);
		String roleName = getPara("roleName", "");

		Page<AdminAuthority> adminAuthority = AdminAuthority.dao.paginate(page, rows, roleName);

		setAttr("info", "success");
		setAttr("rows", adminAuthority.getList());
		setAttr("total", adminAuthority.getTotalRow());
		renderJson(new String[]{"info", "rows", "total"});
	}
	
	// 权限数据添加
	public void adminAuthorityAdd(){

		String roleName = getPara("roleName", "");
		// 权限列表
		String user = getPara("user", "");
		String adminUser = getPara("adminUser", "");
		String adminAuthority = getPara("adminAuthority", "");
		
		if (roleName == "") {
			renderJson("info", "请输入角色姓名！");
			return;
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("user", user);
		map.put("adminUser", adminUser);
		map.put("adminAuthority", adminAuthority);

		AdminAuthority model = new AdminAuthority();
		model.setRoleName(roleName);
		model.setAuthority(JsonKit.toJson(map));
		model.setStatus(0);
		model.setCreateTime(new Date());
		if (!AdminAuthority.dao.adminAuthorityAdd(model)) {
			renderJson("info", "sql数据错误！");
			return;
		};

		setAttr("info", "success");
		renderJson(new String[]{"info"});
	}
	
	// 权限状态修改
	public void adminAuthoritySaveStatus(){
		
		Integer id = getParaToInt("id", 0);
		Integer num = getParaToInt("num", 0);
		
		if (id <= 0) {
			renderJson("info", "数据错误！");
			return;
		}
		
		AdminAuthority model = new AdminAuthority();
		model.setId(id);
		model.setStatus(num);
		if (!AdminAuthority.dao.adminAuthoritySaveData(model)) {
			renderJson("info", "sql数据错误！");
			return;
		};
		
		setAttr("info", "success");
		renderJson(new String[]{"info"});
	}
	
	// 权限id查询
	public void adminAuthorityIdSelect(){
		
		int id = getParaToInt("id", 0);
		
		if (id <= 0) {
			renderJson("info", "数据错误！");
			return;
		}
		
		AdminAuthority adminAuthority = AdminAuthority.dao.adminAuthorityIdSelect(id);

		setAttr("info", "success");
		setAttr("rows", adminAuthority);
		renderJson(new String[]{"info", "rows"});
	}
	
	// 权限数据修改
	public void adminAuthoritySaveData(){

		String roleName = getPara("roleName", "");
		
		// 权限列表
		String user = getPara("user", "");
		String adminUser = getPara("adminUser", "");
		String adminAuthority = getPara("adminAuthority", "");
				
		Integer id = getParaToInt("id", 0);
		
		if (roleName == "") {
			renderJson("info", "请输入角色姓名！");
			return;
		}
		if (id <= 0) {
			renderJson("info", "数据错误！");
			return;
		}
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("user", user);
		map.put("adminUser", adminUser);
		map.put("adminAuthority", adminAuthority);
		
		AdminAuthority model = new AdminAuthority();
		model.setId(id);
		model.setAuthority(JsonKit.toJson(map));
		if (!AdminAuthority.dao.adminAuthoritySaveData(model)) {
			renderJson("info", "sql数据错误！");
			return;
		};
		
		setAttr("info", "success");
		renderJson(new String[]{"info"});
	}
	
	// 权限列表显示
	public void adminAuthorityList(){
		
		List<AdminAuthority> adminAuthority = AdminAuthority.dao.adminAuthorityList();
		
		setAttr("info", "success");
		setAttr("rows", adminAuthority);
		renderJson(new String[]{"info", "rows"});
	}
}
