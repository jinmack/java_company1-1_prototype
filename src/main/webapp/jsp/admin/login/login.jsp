<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<jsp:include page="/jsp/admin/common/head.jsp"></jsp:include>

<div id='login' style="background: url(common/images/Amazing_Landscape_103.jpg) no-repeat center;">
	<div id='login-01'>
		<form action="" onsubmit="return false;" role="form">
			<div id='zhong'>
				<p>企业管理系统${aa }</p>
				<input type="text" class="form-control" id="user" placeholder="请输入您的帐号！">
				<input type="password" class="form-control" id='password' placeholder="请输入您的密码！">
				<input type="text" class="form-control" id='code' placeholder="校验码！">
				<span id='prompt-img'></span>
				<input class='submit btn btn-default btn-sm' type="submit" value="提交" id='submit'>
				<input class='submit btn btn-default btn-sm' type="submit" value='刷新' id='refresh'>
			</div>

		</form>
	</div>
</div>

<script type="text/javascript">

	$("#prompt-img").empty().append("<img id='code-img' style='width:100px;height:40px;' src='/i/code?time="+Math.random()+"'  />");
	$("#code-img").click(function(){ $(this).attr({ src: '/i/code?time='+Math.random() }) });

	$("#submit").click(function(){ 

		var user = $("#user").val();
		var password = $("#password").val();
		var code = $("#code").val();
		
		$.messager.progress({msg:'正在执行...',interval:3000,});
		$.ajax({
			type:"post",
			url:"/admin/loginC",
			data:{
				user:user,
				password:password,
				code:code,
			},
			cache: false,
			dataType:'html',
			success:function(data){

				var data = eval('('+data+')');
				if( data.info == 'success' ){
					
					$.messager.progress('close');
					window.location.href="/admin/main";
				} else {
					
					$.messager.progress('close');
					$.messager.alert('提示信息', data.info);
				}
			}
		});

	});

	$("#refresh").click(function(){ 
		$("#prompt-img").empty().append("<img id='code-img' style='width:100px;height:40px;' src='/i/code?time="+Math.random()+"'  />");
		$("#code-img").click(function(){ $(this).attr({ src: '/i/code?time='+Math.random() }) });
	});

</script>

<jsp:include page="/jsp/admin/common/tail.jsp"></jsp:include>
