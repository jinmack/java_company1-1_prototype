(function(){

	product = {
		//刷新列表
		reload:function(){
			$('#dataListShow').datagrid('reload');
		},
		//进度
		progress:function(msg){
			var win = $.messager.progress({
				msg:msg,
				interval:3000,
				});
		},
		//操作成功提醒
		successShow:function(msg){
			$.messager.show({
				title : '提示',
				msg : msg,
				timeout:3000,
			});
		},
		//删除图片
		removePic:function(){
			$("body").on('mouseover mouseout','.picShowWrap',function(element){
				if(event.type == "mouseover"){
				//鼠标进入	
				$(this).find('.removePicBtn').show();
				
				  }else if(event.type == "mouseout"){
				  //鼠标离开
				 	$(this).find('.removePicBtn').hide();

				  }
			})
			$("body").on('click','.removePicBtn',function(element){
				$(this).parent().remove();
			})
		},
		//上传图片并显示
		upPic:function(el){
				var filename = $(el).val();
				var _this = $(el);
				if (!/\.(gif|jpg|png|jpeg|bmp)$/i.test(filename)) {
					$.messager.alert("提醒","目前只接受gif、jpg、png格式的图片上传。");
					return;	
				 }
				id = $(el).attr('id')
				product.progress('正在上传....');
				$.ajaxFileUpload({
					url:appPath+'/uploadPic',
					secureuri:true,
					fileElementId:id,
					dataType:'json',
					success:function(data){
						$.messager.progress('close');
						var json = eval(data).Pic[0] ;
						var url=json.picUrl;
						var picId = json.picId;
						var picKey = json.picKey
						$('#'+id+'Child').append("<div class='picShowWrap' style='width:100px;height:100px;position:relative;display:inline-block'>"
												+"<img class='"+id+"Child' data-id="+picId+" data-picKey="+picKey+" data-width="+json.width+" data-height="+json.height+"  width='100' height='100'"
												+" src='"+url+"@100w_100h.jpg'/>"
												+"<div class='removePicBtn' style='position:absolute;right:10px;top:10px;display:none'><img src='../../assets/img/common/remove.png'></div>"
												+"<input style='position: absolute;left:0px;bottom:0px' type='button' onclick='product.removePre(this)' value='左移' />"
												+"<button type='button' class='btn btn-danger btn-xs' style='padding:0;position: absolute;right: 0;'; onclick='removePic7(this)'><span class='glyphicon glyphicon-remove' style='margin:2px;'></span></button>"
												+"</div>");
					 },
					error:function(data){
						$.messager.progress('close');
						$.messager.alert('提醒','上传失败');
					}
				})
		},
		//上传图片并显示-蒋-160801-普通input图片上传
		upPicJiang:function(el){
			var filename = $(el).val();
			var _this = $(el);
			if (!/\.(gif|jpg|png|jpeg|bmp)$/i.test(filename)) {
				$.messager.alert("提醒","目前只接受gif、jpg、png格式的图片上传。");
				return;	
			 }
			id = $(el).attr('id')
			product.progress('正在上传....');
			$.ajaxFileUpload({
				url:appPath+'/uploadPic',
				secureuri:true,
				fileElementId:id,
				dataType:'json',
				success:function(data){
					$.messager.progress('close');
					var json = eval(data).Pic[0] ;
					var url = json.picUrl;
					var picId = json.picId;
					var picKey = json.picKey
					var width = json.width;
					var height = json.height;

					$('#'+id+'Child').append("<div class='picShowWrap' style='width:200px;height:auto;position:relative;display:inline-block; float:left;'>"
											+"<img class='productPic2Child' data-id="+picId+" data-picKey="+picKey+" data-width="+width+" data-height="+height+"  width='250' height='150'"
											+" src='"+url+"@300w_150h.jpg'/>"
											+"<div class='removePicBtn' style='position:absolute;right:10px;top:10px;display:none'><img src='../../assets/img/common/remove.png'></div>"
											+"<button type='button' class='btn btn-danger btn-xs' style='padding:0;position: absolute;right: 0;'; onclick='removePic2(this)'><span class='glyphicon glyphicon-remove' style='margin:2px;'></span></button>"
											+"</div>");

					// 160728. 小蒋增加
					$("#myfile-bg").hide();
				 },
				error:function(data){
					$.messager.progress('close');
					$.messager.alert('提醒','上传失败');
				}
			})
		},
		//上传图片并显示 - 蒋-160801-百度编辑器 - 测试 - 无用
		upPicJiangV2:function(el){
				var filename = $(el).val();
				var _this = $(el);
				if (!/\.(gif|jpg|png|jpeg|bmp)$/i.test(filename)) {
					$.messager.alert("提醒","目前只接受gif、jpg、png格式的图片上传。");
					return;	
				 }
				id = $(el).attr('id')
				product.progress('正在上传....');
				$.ajaxFileUpload({
					url:appPath+'/uploadPic',
					secureuri:true,
					fileElementId:id,
					dataType:'json',
					success:function(data){
						$.messager.progress('close');
						var json = eval(data).Pic[0] ;
						var url=json.picUrl;
						var picId = json.picId;
						var picKey = json.picKey
						var width = json.width;
						var height = json.height;
						$('#'+id+'Child').append("<div class='picShowWrap' style='width:100px;height:100px;position:relative;display:inline-block'>"
												+"<img class='"+id+"Child' data-id="+picId+" data-picKey="+picKey+" data-width="+json.width+" data-height="+json.height+"  width='100' height='100'"
												+" src='"+url+"@100w_100h.jpg'/>"
												+"<div class='removePicBtn' style='position:absolute;right:10px;top:10px;display:none'><img src='../../assets/img/common/remove.png'></div>"
												+"<button type='button' class='btn btn-danger btn-xs' style='padding:0;position: absolute;right: 0;'; onclick='removePic7(this)'><span class='glyphicon glyphicon-remove' style='margin:2px;'></span></button>"
												+"</div>");

					 },
					error:function(data){
						$.messager.progress('close');
						$.messager.alert('提醒','上传失败');
					}
				})
		},
		//多图片上传 - 160815
		upPicJiangV3:function(el){


				var reg = new RegExp("(^|&)supplierId=([^&]*)(&|$)");
				var supplierId = window.location.search.substr(1).match(reg);
				var supplierId = supplierId[2];

				var filename = $(el).val();
				var _this = $(el);
				if (!/\.(gif|jpg|png|jpeg|bmp)$/i.test(filename)) {
					$.messager.alert("提醒","目前只接受gif、jpg、png格式的图片上传。");
					return;	
				 }
				id = $(el).attr('id')
				product.progress('正在上传....');
				$.ajaxFileUpload({
					url:appPath+'/uploadLocalImageV3?supplierId='+supplierId,
					secureuri:true,
					fileElementId:id,
					dataType:'json',
					success:function(data){
						$.messager.progress('close');
						var json = eval(data).Pic[0] ;
						var url=json.picUrl;
						var picId = json.picId;
						var productId = json.productId;
						var picKey = json.picKey;
						var width = json.width;
						var height = json.height;
						var seq = json.seq;
						var status = json.status;
						$('#'+id+'Child').append("<div class='picShowWrap' style='width:100px;height:100px;position:relative;display:inline-block'>"
												+"<img class='"+id+"Child' style='width:100px;height:100px;' data-status="+status+" data-seq="+seq+" data-productId="+productId+" data-id="+picId+" data-picKey="+picKey+" data-width="+width+" data-height="+height+"  width='100' height='100'"
												+" src='"+url+"@100w_100h.jpg'/>"
												+"<div class='removePicBtn' style='position:absolute;right:10px;top:10px;display:none'><img src='../../assets/img/common/remove.png'></div>"
												+"<input style='position: absolute;left:0px;bottom:0px' type='button' onclick='product.removePre(this)' value='左移' />"
												+"<button type='button' class='btn btn-danger btn-xs' style='padding:0;position: absolute;right: 0;'; onclick='removePic7(this)'><span class='glyphicon glyphicon-remove' style='margin:2px;'></span></button>"
												+"</div>");
					 },
					error:function(data){
						$.messager.progress('close');
						$.messager.alert('提醒','上传失败');
					}
				})
		},
		//多图片上传 - 160921 - 供应商
		upPicJiangV4:function(el, companyId){

			//供应商id获取
			var supplierId = companyId;

			var filename = $(el).val();
			var _this = $(el);
			if (!/\.(gif|jpg|png|jpeg|bmp)$/i.test(filename)) {
				$.messager.alert("提醒","目前只接受gif、jpg、png格式的图片上传。");
				return;	
			}
			id = $(el).attr('id')
			product.progress('正在上传....');
			$.ajaxFileUpload({
				url:'/uploadLocalImageV3?supplierId='+supplierId,
				secureuri:true,
				fileElementId:id,
				dataType:'json',
				success:function(data){
					$.messager.progress('close');
					var json = eval(data).Pic[0];
					var url=json.picUrl;
					var picId = json.picId;
					var productId = json.productId;
					var picKey = json.picKey;
					var width = json.width;
					var height = json.height;
					var seq = json.seq;
					var status = json.status;
					$('#'+id+'Child').append("<div class='picShowWrap' style='width:100px;height:100px;position:relative;display:inline-block'>"
											+"<img class='"+id+"Child' style='width:100px;height:100px;' data-status="+status+" data-seq="+seq+" data-productId="+productId+" data-id="+picId+" data-picKey="+picKey+" data-width="+width+" data-height="+height+"  width='100' height='100'"
											+" src='"+url+"@100w_100h.jpg'/>"
											+"<div class='removePicBtn' style='position:absolute;right:10px;top:10px;display:none'><img src='../../assets/img/common/remove.png'></div>"
											+"<input style='position: absolute;left:0px;bottom:0px' type='button' onclick='product.removePre(this)' value='左移' />"
											+"<button type='button' class='btn btn-danger btn-xs' style='padding:0;position: absolute;right: 0;'; onclick='removePic7(this)'><span class='glyphicon glyphicon-remove' style='margin:2px;'></span></button>"
											+"</div>");
				 },
				error:function(data){
					$.messager.progress('close');
					$.messager.alert('提醒','上传失败');
				}
			})
		},
		//上传图片并显示-蒋-161011-普通input图片上传-一个页面多个单独图片上传
		upPicJiangV5:function(el){
			var filename = $(el).val();
			var _this = $(el);
			if (!/\.(gif|jpg|png|jpeg|bmp)$/i.test(filename)) {
				$.messager.alert("提醒","目前只接受gif、jpg、png格式的图片上传。");
				return;	
			 }
			id = $(el).attr('id')
			var num = id.replace(/[^0-9]/ig,""); 
			product.progress('正在上传....');
			$.ajaxFileUpload({
				url:'/uploadPic',
				secureuri:true,
				fileElementId:id,
				dataType:'json',
				success:function(data){
					$.messager.progress('close');
					var json = eval(data).Pic[0] ;
					var url = json.picUrl;
					var picId = json.picId;
					var picKey = json.picKey
					var width = json.width;
					var height = json.height;

					$('#'+id+'Child').append("<div class='picShowWrap' style='width:200px;height:auto;position:relative;display:inline-block; float:left;'>"
											+"<img class='productPic"+num+"Child' data-id="+picId+" data-picKey="+picKey+" pickey="+picKey+" data-width="+width+" data-height="+height+"  width='250' height='150'"
											+" src='"+url+"@300w_150h.jpg'/>"
											+"<div class='removePicBtn' style='position:absolute;right:10px;top:10px;display:none'><img src='../../assets/img/common/remove.png'></div>"
											+"<button type='button' class='btn btn-danger btn-xs' style='padding:0;position: absolute;right: 0;'; onclick='removePic"+num+"(this)'><span class='glyphicon glyphicon-remove' style='margin:2px;'></span></button>"
											+"</div>");

					// 161011. 小蒋增加
					$("#myfile-bg"+num).hide();
				 },
				error:function(data){
					$.messager.progress('close');
					$.messager.alert('提醒','上传失败');
				}
			})
		},

		removePre:function(el){
			var index = $(el).parent().index() ;
			if(index != 0){
				var str1 = $(el).parent().html();
				var str2 = $(el).parent().parent().find(".picShowWrap").eq(index-1).html();
				$(el).parent().parent().find(".picShowWrap").eq(index-1).html(str1);
				 $(el).parent().html(str2);
			}else{
				alert("已经是首位了！");
			}
		},

	};
	/**
	 * 模拟java里的Map
	 */
	makeMap = {
		Map:function (){
			// private 
			var obj = {} ;// 空的对象容器,承装键值对
			
			// put 方法
			this.put = function(key , value){
					obj[key] = value ;		// 把键值对绑定到obj对象上
			}
			
			// size 方法 获得map容器的个数
			this.size = function(){
					var count = 0 ; 
					for(var attr in obj){
						count++;
					}
					return count ; 
			}
			
			// get 方法 根据key 取得value
			this.get = function(key){
				if(obj[key] || obj[key] === 0 || obj[key] === false){
					return obj[key];
				} else {
					return null;
				}
			}
			
			//remove 删除方法
			this.remove = function(key){
				if(obj[key] || obj[key] === 0 || obj[key] === false){
					delete obj[key];						
				}
			}
			
			// eachMap 变量map容器的方法
			this.eachMap = function(fn){
					for(var attr in obj){
						fn(attr, obj[attr]);
					}
			}
		}
	};
	domHandle = {
		Height:function(){return document.documentElement.clientHeight},
		Width:function(){return document.documentElement.clientWidth},
	};
	
	// 160907 - 老功能 - 可能作废
	Address = {
		/**
		 * 获取地址数据存储在页面并且赋值
		 */
		getFirstAddressData:function(firstSeletct,secondSelect){

			$.get('/getRegionsNameList').done(function(data){
				var json = eval('('+data+')') ;
				var regionsList = json.firstRegionsList;
				if(regionsList.length >0){
					for(var i = 0 ; i<regionsList.length ; i++){
						var str = "<option value='"+regionsList[i].id+"' >"+regionsList[i].name+"</option>" ;
						$("."+firstSeletct).append(str);
					}
				};
				var regionsChildList = regionsList[0].childRegionsList ;
				if(regionsChildList.length >0){
					for(var i = 0 ; i<regionsChildList.length ; i++){
						var str = "<option value='"+regionsChildList[i].id+"' >"+regionsChildList[i].name+"</option>" ;
						$("."+secondSelect).append(str);
					}
				}
			});
		},
		/****
		 * 二级选择框随一级变动
		 */
		getAddressChildNameList:function(el){

			var index = $(el).val() ;
			var secondSelect = $(el).attr("secondSelect");
			$("."+secondSelect).children().remove();
			$.get('/getRegionsNameList').done(function(data){
				var json = eval('('+data+')');
				var categoryList = json.firstRegionsList;
				for(var a = 0 ; a<categoryList.length;a++){
					if(categoryList[a].id == index){
						var categoryChildList = json.firstRegionsList[a].childRegionsList;
					if(categoryChildList.length >0){
								for(var i = 0 ; i<categoryChildList.length ; i++){
									var str = "<option value='"+categoryChildList[i].id+"' >"+categoryChildList[i].name+"</option>" ;
									$("."+secondSelect).append(str);
								}
							};
						return;
					}
				}
			});
			
		},
		/****
		 * 修改时候赋值
		 */
		SetDefault:function(firstSeletct,secondSelect,pid,id){
			$.get('/getRegionsNameList').done(function(data){
				var json = eval('('+data+')') ;
				var regionsList = json.firstRegionsList;
				if(regionsList.length >0){
					for(var i = 0 ; i<regionsList.length ; i++){
						var str = "<option value='"+regionsList[i].id+"' >"+regionsList[i].name+"</option>" ;
						$("."+firstSeletct).append(str);
					}
				};
				$("."+firstSeletct).val(pid);
				for(var a = 0 ; a<regionsList.length;a++){
					if(regionsList[a].id == pid){
						var regionsChildList = regionsList[a].childRegionsList;
						if(regionsChildList.length >0){
							for(var i = 0 ; i<regionsChildList.length ; i++){
								var str = "<option value='"+regionsChildList[i].id+"' >"+regionsChildList[i].name+"</option>" ;
								$("."+secondSelect).append(str);
							}
						};
						 break;
					}
				}
				$("."+secondSelect).val(id);
				
			});
		}

	}

	// 160808 - 全局搜索 - 测试 - 作废
	AddressV2 = {
		/**
		 * 获取地址数据存储在页面并且赋值
		 */
		getFirstAddressData:function(firstSeletct,secondSelect){

			$.get('/getRegionsNameList').done(function(data){
				var json = eval('('+data+')') ;
				var regionsList = json.firstRegionsList;
				if(regionsList.length >0){

					// $("."+firstSeletct).append("<option value='-1000' >全部</option>");
					for(var i = 0 ; i<regionsList.length ; i++){
						var str = "<option value='"+regionsList[i].id+"' >"+regionsList[i].name+"</option>" ;
						$("."+firstSeletct).append(str);
					}
				};
				var regionsChildList = regionsList[0].childRegionsList ;
				if(regionsChildList.length >0){

					// $("."+secondSelect).append("<option value='-100' >全部</option>");
					for(var i = 0 ; i<regionsChildList.length ; i++){
						var str = "<option value='"+regionsChildList[i].id+"' >"+regionsChildList[i].name+"</option>" ;
						$("."+secondSelect).append(str);
					}
				}
			});
		},
		/****
		 * 二级选择框随一级变动
		 */
		getAddressChildNameList:function(el){

			var index = $(el).val() ;
			var secondSelect = $(el).attr("secondSelect");
			$("."+secondSelect).children().remove();
			$.get('/getRegionsNameList').done(function(data){
				var json = eval('('+data+')');
				var categoryList = json.firstRegionsList;

				// $("."+secondSelect).append("<option value='-100' >全部</option>");
				for(var a = 0 ; a<categoryList.length;a++){
					if(categoryList[a].id == index){
						var categoryChildList = json.firstRegionsList[a].childRegionsList;
					if(categoryChildList.length >0){
								for(var i = 0 ; i<categoryChildList.length ; i++){
									var str = "<option value='"+categoryChildList[i].id+"' >"+categoryChildList[i].name+"</option>" ;
									$("."+secondSelect).append(str);
								}
							};
						return;
					}
				}
			});
			
		},
		/****
		 * 修改时候赋值
		 */
		SetDefault:function(firstSeletct,secondSelect,pid,id){
			$.get('/getRegionsNameList').done(function(data){
				var json = eval('('+data+')') ;
				var regionsList = json.firstRegionsList;
				if(regionsList.length >0){
					for(var i = 0 ; i<regionsList.length ; i++){
						var str = "<option value='"+regionsList[i].id+"' >"+regionsList[i].name+"</option>" ;
						$("."+firstSeletct).append(str);
					}
				};
				$("."+firstSeletct).val(pid);
				for(var a = 0 ; a<regionsList.length;a++){
					if(regionsList[a].id == pid){
						var regionsChildList = regionsList[a].childRegionsList;
						if(regionsChildList.length >0){
							for(var i = 0 ; i<regionsChildList.length ; i++){
								var str = "<option value='"+regionsChildList[i].id+"' >"+regionsChildList[i].name+"</option>" ;
								$("."+secondSelect).append(str);
							}
						};
						 break;
					}
				}
				$("."+secondSelect).val(id);
				
			});
		}

	}

	// 160809 - 全局搜索 - 可用
	AddressV3 = {

		/**
		 * 获取地址数据存储在页面并且赋值
		 */
		getFirstAddressData:function(secondSelect){

			$.get('/getRegionsStateOpen').done(function(data){

				var json = eval('('+data+')') ;
				var list = json.list;
				if(list.length >0){

					$("."+secondSelect).append("<option value='-1000'>全部</option>");
					for(var i = 0 ; i<list.length ; i++){
						var str = "<option value='"+list[i].id+"' >"+list[i].name+"</option>" ;
						$("."+secondSelect).append(str);
					}
					$.cookie('cityId', -1000, { expires: 7, path: '/' }); 
				};
			});
		},

	}

	// 160813 - 产品分类 - 作废
	CategoryClassifyV1 = {

		/**
		 * 获取城市cookie，查询产品分类
		 */
		getSellorderCategoryListV1:function(secondSelect, value, ids, cityPid, cityId){

			if(value == null){
				value = 1;
			}

			if(cityId == null){
				cityId = 440300;
			}

			$.get("/getSellorderCategoryListV1", { cityId: cityId },
				function(data){

					var json = eval('('+data+')') ;
					var list = json.list;
					if(list.length >0){
						for(var i = 0 ; i<list.length ; i++){

							if(list[i].id == value){
								var str = "<option value='"+list[i].id+"' selected='selected' >"+list[i].name+"</option>" ;
								$("."+secondSelect).append(str);
							} else {
								var str = "<option value='"+list[i].id+"' >"+list[i].name+"</option>" ;
								$("."+secondSelect).append(str);
							}
						}
					};

					// 顺序加载城市级联数据 - 160816
					/*if(ids == null){
						Address.getFirstAddressData("categoryName5","categoryNameChird5");
					}
					if(ids != null){
						Address.SetDefault("categoryName5", "categoryNameChird5", cityPid, cityId);
					}*/
				}
			);
		},
		
	}

	// 160826 - 产品分类 - 3级
	CategoryClassifyV2 = {

		/**
		 * 默认-查询产品分类
		 */
		getMultilevelSellorderCategoryV2:function(){

			$("#categoryName3Chird").hide();
			$("#categoryName3Chird").empty();
			$("#categoryName3ChirdT2").hide();
			$("#categoryName3ChirdT2").empty();
			$("#categoryName3ChirdT3").hide();
			$("#categoryName3ChirdT3").empty();

			$.get("/getMultilevelSellorderCategory",
				function(data){

					var json = eval('('+data+')') ;
					var list = json.list;
					
					if(list.length >0){
						for(var i = 0; i<list.length; i++){
							$("#categoryName3Chird").show();
							var str = "<option value='"+list[i].id+"' selected='selected' >"+list[i].name+"</option>" ;
							$("#categoryName3Chird").append(str);
						}
					};
					// $("#categoryName3Chird").val(4); // 180627 - 测试，当默认选中可联动2，3级出现的值
					$('#categoryName3Chird').prop('selectedIndex', 0);
					var oneData = $("#categoryName3Chird").val();
					if(oneData != null) $("#categoryName3Chird").attr("classifyData", oneData);

					if(list.length >0){
						for(var i = 0; i<list.length; i++){
							if(list[i].id == oneData){
								for(var m = 0; m<list[i].twoData.length - 1; m++){
									$("#categoryName3ChirdT2").show();
									var str = "<option value='"+list[i].twoData[m].id+"' selected='selected' >"+list[i].twoData[m].name+"</option>" ;
									$("#categoryName3ChirdT2").append(str);
								}
							}
						}
					}
					$('#categoryName3ChirdT2').prop('selectedIndex', 0);
					var twoData = $("#categoryName3ChirdT2").val();
					if(twoData != null) $("#categoryName3Chird").attr("classifyData", twoData);

					if(list.length >0){
						for(var i = 0; i<list.length; i++){
							if((list[i].twoData.length - 1) > 0){
								for(var m = 0; m<list[i].twoData.length - 1; m++){
									if(list[i].twoData[m].id == twoData){
										for(var k = 0; k<list[i].twoData[m].threeData.length - 1; k++){
											$("#categoryName3ChirdT3").show();
											var str = "<option value='"+list[i].twoData[m].threeData[k].id+"' selected='selected' >"+list[i].twoData[m].threeData[k].name+"</option>" ;
											$("#categoryName3ChirdT3").append(str);
										}
									}
								}
							}
						}
					}
					$('#categoryName3ChirdT3').prop('selectedIndex', 0);
					var threeData = $("#categoryName3ChirdT3").val();
					if(threeData != null) $("#categoryName3Chird").attr("classifyData", threeData);

				}
			);
		},
		/**
		 * 一级变动二级，可能联动三级
		 */
		getOneToTwoV2:function(){

			$.get("/getMultilevelSellorderCategory",
				function(data){

					var json = eval('('+data+')') ;
					var list = json.list;

					$("#categoryName3ChirdT2").hide();
					$("#categoryName3ChirdT2").empty();
					$("#categoryName3ChirdT3").hide();
					$("#categoryName3ChirdT3").empty();
					var oneData = $("#categoryName3Chird").val();
					if(oneData != null) $("#categoryName3Chird").attr("classifyData", oneData);

					if(list.length >0){
						for(var i = 0; i<list.length; i++){
							if(list[i].id == oneData){
								for(var m = 0; m<list[i].twoData.length - 1; m++){
									$("#categoryName3ChirdT2").show();
									var str = "<option value='"+list[i].twoData[m].id+"' selected='selected' >"+list[i].twoData[m].name+"</option>" ;
									$("#categoryName3ChirdT2").append(str);
								}
							}
						}
					}
					$('#categoryName3ChirdT2').prop('selectedIndex', 0);
					var twoData = $("#categoryName3ChirdT2").val();
					if(twoData != null) $("#categoryName3Chird").attr("classifyData", twoData);

					if(list.length >0){
						for(var i = 0; i<list.length; i++){
							if((list[i].twoData.length - 1) > 0){
								for(var m = 0; m<list[i].twoData.length - 1; m++){
									if(list[i].twoData[m].id == twoData){
										for(var k = 0; k<list[i].twoData[m].threeData.length - 1; k++){
											$("#categoryName3ChirdT3").show();
											var str = "<option value='"+list[i].twoData[m].threeData[k].id+"' selected='selected' >"+list[i].twoData[m].threeData[k].name+"</option>" ;
											$("#categoryName3ChirdT3").append(str);
										}
									}
								}
							}
						}
					}
					$('#categoryName3ChirdT3').prop('selectedIndex', 0);
					var threeData = $("#categoryName3ChirdT3").val();
					if(threeData != null) $("#categoryName3Chird").attr("classifyData", threeData);

				}
			);
		},
		/**
		 * 二级变动三级
		 */
		getTwoToThreeV2:function(){

			$.get("/getMultilevelSellorderCategory",
				function(data){

					var json = eval('('+data+')') ;
					var list = json.list;

					$("#categoryName3ChirdT3").hide();
					$("#categoryName3ChirdT3").empty();
					var twoData = $("#categoryName3ChirdT2").val();
					if(twoData != null) $("#categoryName3Chird").attr("classifyData", twoData);

					if(list.length >0){
						for(var i = 0; i<list.length; i++){
							if((list[i].twoData.length - 1) > 0){
								for(var m = 0; m<list[i].twoData.length - 1; m++){
									if(list[i].twoData[m].id == twoData){
										for(var k = 0; k<list[i].twoData[m].threeData.length - 1; k++){
											$("#categoryName3ChirdT3").show();
											var str = "<option value='"+list[i].twoData[m].threeData[k].id+"' selected='selected' >"+list[i].twoData[m].threeData[k].name+"</option>" ;
											$("#categoryName3ChirdT3").append(str);
										}
									}
								}
							}
						}
					}
					$('#categoryName3ChirdT3').prop('selectedIndex', 0);
					var threeData = $("#categoryName3ChirdT3").val();
					if(threeData != null) $("#categoryName3Chird").attr("classifyData", threeData);

				}
			);
		},
		/**
		 * 三级变动，赋值
		 */
		getThreeToValueV2:function(){

			var threeData = $("#categoryName3ChirdT3").val();
			if(threeData != null) $("#categoryName3Chird").attr("classifyData", threeData);

		},
		/**
		 * 修复，默认值操作
		 */
		updateV2:function(categoryId){

			var categoryId = categoryId;
			$("#categoryName3Chird").attr("classifyData", categoryId);
			$("#categoryName3Chird").hide();
			$("#categoryName3Chird").empty();
			$("#categoryName3ChirdT2").hide();
			$("#categoryName3ChirdT2").empty();
			$("#categoryName3ChirdT3").hide();
			$("#categoryName3ChirdT3").empty();

			$.get("/getMultilevelSellorderCategory",
				function(data){

					$("#categoryName3Chird").hide();
					$("#categoryName3Chird").empty();

					var json = eval('('+data+')') ;
					var list = json.list;
					
					// 一级
					if(list.length >0){
						for(var i = 0; i<list.length; i++){
							if(list[i].id == categoryId){
								for(var a = 0; a<list.length; a++){
									$("#categoryName3Chird").show();
									var str = "<option value='"+list[a].id+"' selected='selected' >"+list[a].name+"</option>" ;
									$("#categoryName3Chird").append(str);
								}
								$("#categoryName3Chird").val(categoryId);
							}
						}
					};

					// 二级
					if(list.length >0){
						for(var i = 0; i<list.length; i++){
							for(var m = 0; m<list[i].twoData.length - 1; m++){
								if(list[i].twoData[m].id == categoryId){
									var twoDataV2 = categoryId;
									for(var a = 0; a<list.length; a++){
										$("#categoryName3Chird").show();
										var str = "<option value='"+list[a].id+"' selected='selected' >"+list[a].name+"</option>" ;
										$("#categoryName3Chird").append(str);
									}
									$("#categoryName3Chird").val(list[i].twoData[m].pid);
								}
								if(twoDataV2 != null){
									$("#categoryName3ChirdT2").show();
									var str = "<option value='"+list[i].twoData[m].id+"' selected='selected' >"+list[i].twoData[m].name+"</option>" ;
									$("#categoryName3ChirdT2").append(str);
								}
							}
							console.log("33333");
							if(twoDataV2 != null) $("#categoryName3ChirdT2").val(twoDataV2);
						}
					};

					// 三级
					if(list.length >0){
						for(var i = 0; i<list.length; i++){
							for(var m = 0; m<list[i].twoData.length - 1; m++){
								for(var k = 0; k<list[i].twoData[m].threeData.length - 1; k++){
									if(list[i].twoData[m].threeData[k].id == categoryId){
										var threeDataV3 = categoryId;
										var threeToV3 = list[i].twoData[m].threeData[k].pid;
										for(var a = 0; a<list[i].twoData[m].threeData.length - 1; a++){
											$("#categoryName3ChirdT3").show();
											var str = "<option value='"+list[i].twoData[m].threeData[a].id+"' selected='selected' >"+list[i].twoData[m].threeData[a].name+"</option>" ;
											$("#categoryName3ChirdT3").append(str);
										}
										$("#categoryName3ChirdT3").val(threeDataV3);
									}
								}
								if(list[i].twoData[m].id == threeToV3){
									var twoToV3 = list[i].twoData[m].pid;
									for(var a = 0; a<list[i].twoData.length - 1; a++){
										$("#categoryName3ChirdT2").show();
										var str = "<option value='"+list[i].twoData[a].id+"' selected='selected' >"+list[i].twoData[a].name+"</option>" ;
										$("#categoryName3ChirdT2").append(str);
									}
									$("#categoryName3ChirdT2").val(threeToV3);
								}
							}

							if(twoToV3 != null){
								for(var i = 0; i<list.length; i++){
									$("#categoryName3Chird").show();
									var str = "<option value='"+list[i].id+"' selected='selected' >"+list[i].name+"</option>" ;
									$("#categoryName3Chird").append(str);
								}
								$("#categoryName3Chird").val(twoToV3);
							}
						}
					}
				}
			);
		},

	}

	CategoryClassifyV3 = {
		/**
		 * 获取分类列表
		 */
		getCategoryListV1:function(){

			$.get("/getMultilevelSellorderCategory",
				function(data){

					var json = eval('('+data+')') ;
					var list = json.list;

					$("#categoryName3Chird").empty();

					if(list.length >0){
						for(var i = 0; i<list.length; i++){

							str = "<option value='"+list[i].id+"' >"+list[i].name+"</option>" ;
							$("#categoryName3Chird").append(str);

							for(var m = 0; m<list[i].twoData.length - 1; m++){

								str = "<option value='"+list[i].twoData[m].id+"' >&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].name+"</option>" ;
								$("#categoryName3Chird").append(str);

								for(var k = 0; k<list[i].twoData[m].threeData.length - 1; k++){

									str = "<option value='"+list[i].twoData[m].threeData[k].id+"' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].threeData[k].name+"</option>" ;
									$("#categoryName3Chird").append(str);

								}

							}

						}

					};

				}

			);

		},

		/**
		 * 获取分类列表
		 */
		getCategoryUpdateV1:function(id){

			var id = id;

			$("#categoryName3Chird").empty();

			$.get("/getMultilevelSellorderCategory",
				function(data){

					var json = eval('('+data+')') ;
					var list = json.list;

					$("#categoryName3Chird").empty();

					if(list.length >0){
						for(var i = 0; i<list.length; i++){

							if(list[i].id == id){
								str = "<option value='"+list[i].id+"' selected='selected' >"+list[i].name+"</option>" ;
								$("#categoryName3Chird").append(str);
							} else {
								str = "<option value='"+list[i].id+"' >"+list[i].name+"</option>" ;
								$("#categoryName3Chird").append(str);
							}

							for(var m = 0; m<list[i].twoData.length - 1; m++){

								if(list[i].twoData[m].id == id){
									str = "<option value='"+list[i].twoData[m].id+"' selected='selected' >&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].name+"</option>" ;
									$("#categoryName3Chird").append(str);
								} else {
									str = "<option value='"+list[i].twoData[m].id+"' >&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].name+"</option>" ;
									$("#categoryName3Chird").append(str);
								}

								for(var k = 0; k<list[i].twoData[m].threeData.length - 1; k++){

									// 无限分类把 pid 修改为 id - 160829
									// str = "<option value='"+list[i].twoData[m].threeData[k].id+"' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].threeData[k].name+"</option>" ;
									
									if(list[i].twoData[m].threeData[k].id == id){
										str = "<option value='"+list[i].twoData[m].threeData[k].pid+"' selected='selected' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].threeData[k].name+"</option>" ;
										$("#categoryName3Chird").append(str);
									} else {
										str = "<option value='"+list[i].twoData[m].threeData[k].pid+"' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].threeData[k].name+"</option>" ;
										$("#categoryName3Chird").append(str);
									}
								}
							}
						}
					};

				}

			);

		},

	}

	// 160829 - 其他地方调用 - 分类功能 - 3级
	CategoryManageV1 = {

		/**
		 * 分类列表
		 */
		getCategoryListV1:function(){

			$.get("/getMultilevelSellorderCategory",
				function(data){

					var json = eval('('+data+')') ;
					var list = json.list;

					$("#categoryPid").empty();

					if(list.length >0){
						var str = "<option value='0'>添加顶级分类</option>";
						$("#categoryPid").append(str);
						for(var i = 0; i<list.length; i++){

							str = "<option value='"+list[i].id+"' >"+list[i].name+"</option>" ;
							$("#categoryPid").append(str);

							for(var m = 0; m<list[i].twoData.length - 1; m++){

								str = "<option value='"+list[i].twoData[m].id+"' >&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].name+"</option>" ;
								$("#categoryPid").append(str);

								for(var k = 0; k<list[i].twoData[m].threeData.length - 1; k++){

									// 无限分类把 pid 修改为 id - 160829
									// str = "<option value='"+list[i].twoData[m].threeData[k].id+"' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].threeData[k].name+"</option>" ;
									str = "<option value='"+list[i].twoData[m].threeData[k].pid+"' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].threeData[k].name+"</option>" ;
									$("#categoryPid").append(str);

								}

							}


						}
					};

					Address.getFirstAddressData("categoryName2","categoryNameChird2");
				}
			);
		},
		/**
		 * 分类修改 - 默认值
		 */
		getCategoryUpdateV1:function(id, cityPid, cityId){
			var id = id;
			var cityPid = cityPid;
			var cityId = cityId;

			$("#categoryPid").empty();

			$.get("/getMultilevelSellorderCategory",
				function(data){

					var json = eval('('+data+')') ;
					var list = json.list;

					$("#categoryPid").empty();

					if(list.length >0){
						var str = "<option value='0'>添加顶级分类</option>";
						$("#categoryPid").append(str);
						for(var i = 0; i<list.length; i++){

							if(list[i].id == id){
								str = "<option value='"+list[i].id+"' selected='selected' >"+list[i].name+"</option>" ;
								$("#categoryPid").append(str);
							} else {
								str = "<option value='"+list[i].id+"' >"+list[i].name+"</option>" ;
								$("#categoryPid").append(str);
							}

							for(var m = 0; m<list[i].twoData.length - 1; m++){

								if(list[i].twoData[m].id == id){
									str = "<option value='"+list[i].twoData[m].id+"' selected='selected' >&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].name+"</option>" ;
									$("#categoryPid").append(str);
								} else {
									str = "<option value='"+list[i].twoData[m].id+"' >&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].name+"</option>" ;
									$("#categoryPid").append(str);
								}

								for(var k = 0; k<list[i].twoData[m].threeData.length - 1; k++){

									// 开启无限分类把 pid修改为 id，无限增加 for - 160829
									// str = "<option value='"+list[i].twoData[m].threeData[k].id+"' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].threeData[k].name+"</option>" ;
									
									if(list[i].twoData[m].threeData[k].id == id){
										str = "<option value='"+list[i].twoData[m].threeData[k].pid+"' selected='selected' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].threeData[k].name+"</option>" ;
										$("#categoryPid").append(str);
									} else {
										str = "<option value='"+list[i].twoData[m].threeData[k].pid+"' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+list[i].twoData[m].threeData[k].name+"</option>" ;
										$("#categoryPid").append(str);
									}
								}
							}
						}
					};

					Address.SetDefault("categoryName2", "categoryNameChird2", cityPid, cityId);
				}
			);

		},

	}














})()

