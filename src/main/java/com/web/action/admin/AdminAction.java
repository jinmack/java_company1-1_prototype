package com.web.action.admin;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import com.jfinal.captcha.CaptchaRender;
import com.jfinal.core.ActionKey;
import com.web.remote.dao.AdminAuthority;
import com.web.remote.dao.AdminUser;
import com.web.utils.EncryptstrUtils;

public class AdminAction extends AdminBaseAction {

	public void index() {
		
		// renderText("/admin/index");
		// System.out.println("index");
		// render("index.html");
		renderNull();
		// renderError(404);
	}
	
	// 后台主页面
	public void main(){
		
		HttpSession session = getSession();
		if (getCookie("adminUserId") == null || session.getAttribute("adminAuthority") == null) {
			redirect("/");
		} else {
			renderJsp("/jsp/admin/main/main.jsp");
		}
	}
	
	// 登录主页面
	@ActionKey("/")
	public void login(){
		
		renderJsp("/jsp/admin/login/login.jsp");
	}
	
	// 登录控制层
	public void loginC(){
		
		String user = getPara("user", "");
		String password = getPara("password", "");
		String code = getPara("code", "");
		
		if (user == "") {
			renderJson("info", "请输入手机号！");
			return;
		}
		if (password == "") {
			renderJson("info", "请输入密码！");
			return;
		}
		if (code == "") {
			renderJson("info", "请输入校验码！");
			return;
		}
		boolean loginSuccess = CaptchaRender.validate(this, code);
		if (!loginSuccess) {
			renderJson("info", "验证码错误！");
			return;
		}
		List<AdminUser> loginAdminUser = AdminUser.dao.loginAdminUser(user);
		if (loginAdminUser.isEmpty()) {
			renderJson("info", "无效的邮箱/手机！");
			return;
		}
		if (!loginAdminUser.get(0).getPassword().equals(EncryptstrUtils.encrypt(password))) {
			renderJson("info", "密码错误！");
			return;
		}
		
		List<AdminAuthority> adminAuthority = AdminAuthority.dao.adminAuthorityList();
		for (int j = 0; j < adminAuthority.size(); j++) {
			if (loginAdminUser.get(0).getAdminAuthorityId() == adminAuthority.get(j).getId()) {
				loginAdminUser.get(0).put("adminAuthority", adminAuthority.get(j).getAuthority());
			}
		}

		setCookie("adminUserId", loginAdminUser.get(0).getId().toString(), 86400);
		JSONObject jsonObject = JSONObject.fromObject(loginAdminUser.get(0).get("adminAuthority"));
		Map<String, Object> map = (Map<String, Object>) JSONObject.toBean(jsonObject, Map.class);
		setSessionAttr("adminAuthority", map);
		
		setAttr("info", "success");
		renderJson(new String[]{"info"});
	}
	
	public void out(){
		
		setCookie("adminUserId", "", 0);
		setSessionAttr("adminAuthority", null);
		redirect("/");
	}
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
		//String str = "{\"id\":20,\"createTime\":\"2017-02-22 17:23:12\",\"authority\":\"{\"adminAuthority\":\"true\",\"adminUser\":\"true\",\"user\":\"true\"}\",\"status\":0,\"roleName\":\"管理员\"}";
		/*String str = "{\"adminAuthority\":\"true\",\"adminUser\":\"true\",\"user\":\"true\"}";
		JSONObject jsonObject = JSONObject.fromObject(str);
		Map<String, Object> map = (Map<String, Object>) JSONObject.toBean(jsonObject, Map.class);
		System.out.println(map.get("adminAuthority"));*/
		
		
		
		/*JSONObject jb = JSONObject.fromObject(map);
		System.out.println(jb);
		for (String jb1 : args) {
		}*/
		
		/*String map = "{adminAuthority=true, adminUser=false, user=true}";
		JSONObject  jasonObject = JSONObject.fromObject(map);
		Map str = (Map)jasonObject;*/

		
		
		//System.out.println(str.get("adminAuthority"));
		/*String ss = "{\"adminAuthority\":\"true\",\"adminUser\":\"true\",\"user\":\"true\"}";
		Gson gson = new Gson();
		System.out.println(gson.fromJson(ss, BaseAdminAuthority.class));*/

		
		
		/*DesUtils des = new DesUtils("cps50S2jIIgPVK5uqUChJX0PgC39LLEA");
		System.out.println(des.encryptString("123"));;
		System.out.println(des.encryptString("1234"));;
		System.out.println(des.encryptString("12345"));;
		System.out.println(des.decryptString("fzjbwfjNQkI"));
		System.out.println(des.decryptString("7VayfEhkgx8"));
		System.out.println(des.decryptString("AqzeV97O-F8"));*/

		
	}


}
