<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

	<div id="action" class="easyui-dialog" closed="true" style="display:none;">
		<form class="easyui-form">
			<table>

				<tr>
					<td>角色姓名：</td>
					<td>
						<input name='roleName' id="roleName" type="text" placeholder="输入角色姓名">
					</td>
				</tr>

				<tr>
					<td>会员管理：</td>
					<td class='a1'>
						<table>
							<tr>
								<td>
									用户管理：
								</td>
								<td>
									<input name='user' id='user' type="checkbox">
								</td>
							</tr>
							<tr>
								<td>
									员工管理：
								</td>
								<td>
									<input name='adminUser' id='adminUser' type="checkbox">
								</td>
							</tr>
							<tr>
								<td>
									权限管理：
								</td>
								<td>
									<input name='adminAuthority' id='adminAuthority' type="checkbox">
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<input type="hidden" name="id" id='id'>

			</table>
		</form>

	</div>

	<script type="text/javascript">

		function submit(id){

			var method = "/adminAdminAuthority/adminAuthorityAdd";
			if(id > 0){
				var method = "/adminAdminAuthority/adminAuthoritySaveData";
			}

	        var roleName = $("#roleName").val();
	        var user = $("#user")[0].checked;
	        var adminUser = $("#adminUser")[0].checked;
	        var adminAuthority = $("#adminAuthority")[0].checked;
	        var id = $("#id").val();

	        $.messager.progress({msg:'正在增加...',interval:3000,});
			$.ajax({
				type:"post",
				url:method,
				data:{
					roleName:roleName,
					user:user,
					adminUser:adminUser,
					adminAuthority:adminAuthority,
					id:id,
				},
				cache: false,
				dataType:'html',
				success:function(data){

					var data = eval('('+data+')');
					if( data.info == 'success' ){
						
						$.messager.progress('close');
						$('#action').dialog('close');
						$('#data').datagrid('reload');
					} else {
						
						$.messager.progress('close');
						$.messager.alert('提示信息', data.info);
					}
				}
			});
		}

		function doAdd(id, data){

			$("#action form").form('reset');

			var title = '新增';
			if(id > 0){
				var title = '修改';
				$("#roleName").val(data.roleName);

				var authority = eval('('+data.authority+')');
				if(authority.user == 'true'){
					$("#user").attr("checked",true);
				}else{
					$("#user").attr("checked",false);
				}
				if(authority.adminUser == 'true'){
					$("#adminUser").attr("checked",true);
				}else{
					$("#adminUser").attr("checked",false);
				}
				if(authority.adminAuthority == 'true'){
					$("#adminAuthority").attr("checked",true);
				}else{
					$("#adminAuthority").attr("checked",false);
				}

				$("#id").val(data.id);
			}
			
			$('#action').show();
			$('#action').dialog('open').dialog({
				title:title,    
				width:"100%",    
				height:"100%",
				closed:false,    
				cache:false,    
				modal:true,
				closable:true, 
				left:0,
				top:0,
				buttons:[{
					text:'确定',
					width:'56',
					handler:function(){

						submit(id);
					}
				},{
					text:'取消',
					width:'56',
					handler:function(){

						$('#action').dialog('close');
					}
				}],
			});
		}

	</script>
