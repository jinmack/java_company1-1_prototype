<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<jsp:include page="/jsp/admin/common/head.jsp"></jsp:include>

<c:forEach var="item" items="${adminAuthority }">   

	<c:if test="${item.key == 'user' and item.value == 'true' }">
		<c:set var="adminAdminAuthority" value="1"/>
	</c:if>
	<c:if test="${item.key == 'adminUser' and item.value == 'true' }">
		<c:set var="adminAdminAuthority" value="1"/>
	</c:if>  
	<c:if test="${item.key == 'adminAuthority' and item.value == 'true' }">
		<c:set var="adminAdminAuthority" value="1"/>
	</c:if>  

</c:forEach>  

    <div data-options="region:'north',fit:false" style="overflow-y:hidden;">
    	<!-- <div class='northImg' style="height:40px;background:#aac2f2;"> -->
    	<div class='northImg' style="height:56px;">
    		<span style="padding-left:5px;">企业管理系统</span>
    	</div>
    	<div style="height:25px;background:#f4f4f4;line-height:25px;">
    		<div style="float;left;display: inline;">
    			<!-- <span style="padding-left:5px;">欢迎您：xxx</span> -->
    			<span style="padding-left:10px;" id='time'>今天是：</span>
	    		<script type="text/javascript">
					var d, s = "";
					var x = new Array("星期日", "星期一", "星期二","星期三","星期四", "星期五","星期六");
					d = new Date();
					s+=d.getFullYear()+"年"+(d.getMonth() + 1)+"月"+d.getDate()+"日 ";
					s+=x[d.getDay()];
					$("#time").append(s);
	    		</script>
    		</div>
    		<div style="float:right;display: inline;">
    			<!-- <span style="padding-right:5px;cursor:pointer;">修改密码</span> -->
    			<span style="padding-right:5px;cursor:pointer;" id='fav'>收藏本站</span>
    			<script type="text/javascript">
					function AddFavorite(sURL, sTitle){
					    try{
					        window.external.addFavorite(sURL, sTitle);
					    }
					    catch (e)
						{
					        try
					        {
					            window.sidebar.addPanel(sTitle, sURL, "");
					        }
					        catch (e)
					        {
					            alert("请使用Ctrl+D将本页加入收藏夹！");
					        }
					    }
					}
					window.onload = function(){
						var addFav = document.getElementById("fav");
						addFav.onclick = function(){
							AddFavorite(window.location,document.title);
						}
					}
    			</script>
    			<span style="padding-right:5px;"><a style="" href="/admin/out">退出系统</a></span>
    		</div>
    	</div>
    </div>   
    
    <div data-options="region:'center'">   
        <div class="easyui-layout" data-options="fit:true">
            <div id="west" data-options="region:'west',title:'菜单',split:false" style="width:180px">
				<div style="background:#aaa;" id="nav" class="easyui-accordion" data-options="fit:true,border:false,animate:true">


					<c:if test="${adminAdminAuthority != null }">
						<div title="会员管理" style="padding:0px;" class="nav_menu">
							<ul>
								<c:forEach var="item" items="${adminAuthority }">   

									<c:if test="${item.key == 'adminUser' and item.value == 'true' }">
										<a href="javascript:;" onclick="addTab('员工管理','/adminAdminUser/adminUser')">
											<li>员工管理</li>
										</a>
									</c:if>  

									<c:if test="${item.key == 'adminAuthority' and item.value == 'true' }">
										<a href="javascript:;" onclick="addTab('权限管理','/adminAdminAuthority/adminAuthority')">
											<li>权限管理</li>
										</a>
									</c:if>  

								</c:forEach>
							</ul>
						</div>
					</c:if>


				</div>
            </div>   

			<div data-options="region:'center'">
				<div id="tabs" class="easyui-tabs" data-options="fit:true,border:false,plain:true">

				</div>
			</div>
        </div>   
    </div>
	
	<div id="south" data-options="region:'south',split:false" style="height:25px;text-align: center;line-height:25px;overflow-y:hidden;background:#aac2f2;">Copyright &copy; 2017-2020 <a href="http://www.7cloud.com.cn/" target="_blank">xxx科技有限公司</a> 版权所有</div>   

	<script type="text/javascript">

		// 默认打开欢迎页面
		function mo(){
			
			var title = "欢迎使用";
			var ur = "https://www.baidu.com/";
			var content = '<iframe frameborder="0" src="' + ur + '" style="width:100%;min-width:1024px;height:99%;display:block;"></iframe>';
			$('#tabs').tabs('add', {
				title:title,
				content:content,
				closable:true,
			});
		}
		setTimeout("mo()",100); 

		function addTab(title, url) {

			if ($('#tabs').tabs('exists', title)) {
				$('#tabs').tabs('select', title);
			} else {
				var content = '<iframe frameborder="0" src="'
							+ url +
							'" style="width:100%;min-width:1024px;height:99%;display:block;"></iframe>';
				$('#tabs').tabs('add', {
					title:title,
					content:content,
					closable:true,
				});
			}
		}

	</script>

<jsp:include page="/jsp/admin/common/tail.jsp"></jsp:include>
