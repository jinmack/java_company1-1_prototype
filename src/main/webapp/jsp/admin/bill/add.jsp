<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

	<div id="action" class="easyui-dialog" closed="true" style="display:none;">
		<form class="easyui-form">
			<table class="table-responsive">

				<tr>
					<td>付款单位（人）：</td>
					<td>
						<input style="width:300px" name='paymentUnit' id="paymentUnit" type="text" placeholder="输入付款单位（人）">
					</td>
					<td>日期（自动计算）：</td>
					<td>
						<input name='date' id="date" class="data" type="text" placeholder="输入日期" disabled="true">
					</td>
				</tr>

				<tr>
					<td>收款项目（第一栏）：</td>
					<td>
						<input style="width:300px" name='collectionItem01' id='collectionItem01' type="text" placeholder="输入收款项目（第一栏）">
					</td>
					<td>金额（第一栏）：</td>
					<td>
						<input name='money01' id="money01" type="text" value="0" placeholder="输入金额（第一栏）">
					</td>
				</tr>

				<tr>
					<td>收款项目（第二栏）：</td>
					<td>
						<input style="width:300px" name='collectionItem02' id='collectionItem02' type="text" placeholder="输入收款项目（第二栏）">
					</td>
					<td>金额（第二栏）：</td>
					<td>
						<input name='money02' id="money02" type="text" value="0" placeholder="输入金额（第二栏）">
					</td>
				</tr>

				<tr>
					<td>收款项目（第三栏）：</td>
					<td>
						<input style="width:300px" name='collectionItem03' id='collectionItem03' type="text" placeholder="输入收款项目（第三栏）">
					</td>
					<td>金额（第三栏）：</td>
					<td>
						<input name='money03' id="money03" type="text" value="0" placeholder="输入金额（第三栏）">
					</td>
				</tr>

				<tr>
					<td>总额（小写）（自动计算）：</td>
					<td colspan="3">
						<input style="width:300px" name='totalSmall' id='totalSmall' type="text" value="0" placeholder="输入总额（小写）" disabled="true">
					</td>
				</tr>

				<tr>
					<td>总额（大写）（自动计算）：</td>
					<td colspan="3">
						<input style="width:300px" name='totalBig' id='totalBig' type="text" placeholder="输入总额（大写）" disabled="true">
					</td>
				</tr>

				<tr>
					<td>备注：</td>
					<td colspan="3">
						<input style="width:300px" name='remarks' id='remarks' type="text" placeholder="输入备注">
					</td>
				</tr>

				<tr>
					<td>收款人姓名：</td>
					<td colspan="3">
						<input style="width:300px" name='payeeName' id='payeeName' type="text" placeholder="输入收款人姓名">
					</td>
				</tr>

				<input type="hidden" name="id" id='id'>

			</table>
		</form>

	</div>

	<script type="text/javascript">


	    /** 数字金额大写转换(可以处理整数,小数,负数) */    
	    /*var digitUppercase = function(n) {  
	        var fraction = ['角', '分'];  
	        var digit = [  
	            '零', '壹', '贰', '叁', '肆',  
	            '伍', '陆', '柒', '捌', '玖'  
	        ];  
	        var unit = [  
	            ['元', '万', '亿'],  
	            ['', '拾', '佰', '仟']  
	        ];  
	        var head = n < 0 ? '欠' : '';  
	        n = Math.abs(n);  
	        var s = '';  
	        for (var i = 0; i < fraction.length; i++) {  
	            s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i]).replace(/零./, '');  
	        }  
	        s = s || '整';  
	        n = Math.floor(n);  
	        for (var i = 0; i < unit[0].length && n > 0; i++) {  
	            var p = '';  
	            for (var j = 0; j < unit[1].length && n > 0; j++) {  
	                p = digit[n % 10] + unit[1][j] + p;  
	                n = Math.floor(n / 10);  
	            }  
	            s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s;  
	        }  
	        return head + s.replace(/(零.)*零元/, '元')  
	            .replace(/(零.)+/g, '零')  
	            .replace(/^整$/, '零元整');  
	    };  */

	    /*function DX(n) {
		    if (!/^(0|[1-9]\d*)(\.\d+)?$/.test(n))
		      return "数据非法";
		    var unit = "千百拾亿千百拾万千百拾元角分", str = "";
		      n += "00";
		    var p = n.indexOf('.');
		    if (p >= 0)
		      n = n.substring(0, p) + n.substr(p+1, 2);
		      unit = unit.substr(unit.length - n.length);
		    for (var i=0; i < n.length; i++)
		      str += '零壹贰叁肆伍陆柒捌玖'.charAt(n.charAt(i)) + unit.charAt(i);
		    return str.replace(/零(千|百|拾|角)/g, "零").replace(/(零)+/g, "零").replace(/零(万|亿|元)/g, "$1").replace(/(亿)万|壹(拾)/g, "$1$2").replace(/^元零?|零分/g, "").replace(/元$/g, "元整");
		}*/

		function hs(n){

	        var str = n + "";
	        str = str.replace(/0/g, "零");
	        str = str.replace(/1/g, "壹");
	        str = str.replace(/2/g, "贰");
	        str = str.replace(/3/g, "叁");
	        str = str.replace(/4/g, "肆");
	        str = str.replace(/5/g, "伍");
	        str = str.replace(/6/g, "陆");
	        str = str.replace(/7/g, "柒");
	        str = str.replace(/8/g, "捌");
	        str = str.replace(/9/g, "玖");
	        return str;
		}


		function submit(id){

			var method = "/adminBill/billAuthorityAdd";
			if(id > 0){
				var method = "/adminBill/billSaveData";
			}

	        var paymentUnit = $("#paymentUnit").val();
	        var date = $("#date").val();
	        var collectionItem01 = $("#collectionItem01").val();
	        var money01 = $("#money01").val();
	        var collectionItem02 = $("#collectionItem02").val();
	        var money02 = $("#money02").val();
	        var collectionItem03 = $("#collectionItem03").val();
	        var money03 = $("#money03").val();
	        var totalSmall = $("#totalSmall").val();
	        var totalBig = $("#totalBig").val();
	        var remarks = $("#remarks").val();
	        var payeeName = $("#payeeName").val();

	        var id = $("#id").val();


			if (money01 != 0) {
				if (collectionItem01 == "") {
					$.messager.alert('提示信息', '收款项目（第一栏），请填写文字！');
        			return;
				}
        	}
        	if (money02 != 0) {
        		if (collectionItem02 == "") {
					$.messager.alert('提示信息', '收款项目（第二栏），请填写文字！');
        			return;
				}
        	}
        	if (money03 != 0) {
        		if (collectionItem03 == "") {
					$.messager.alert('提示信息', '收款项目（第三栏），请填写文字！');
        			return;
				}
        	}


	        if (isNaN(money01)) {
	        	$.messager.alert('提示信息', '金额（第一栏），请填写数字！');
	        	return;
	        }
	        if (isNaN(money02)) {
	        	$.messager.alert('提示信息', '金额（第二栏），请填写数字！');
	        	return;
	        }
	        if (isNaN(money03)) {
	        	$.messager.alert('提示信息', '金额（第三栏），请填写数字！');
	        	return;
	        }
	        if (isNaN(totalSmall)) {
	        	$.messager.alert('提示信息', '总额（小写），请填写数字！');
	        	return;
	        }

	        if (money01.length > 7) {
	        	$.messager.alert('提示信息', '填写金额，不能大于7位数！');
	        	return;
	        }

	        if (money02.length > 7) {
	        	$.messager.alert('提示信息', '填写金额，不能大于7位数！');
	        	return;
	        }

	        if (money03.length > 7) {
	        	$.messager.alert('提示信息', '填写金额，不能大于7位数！');
	        	return;
	        }

	        if (totalSmall.length > 7) {
	        	$.messager.alert('提示信息', '小写总额，不能大于7位数！');
	        	return;
	        }

	        if (totalBig.length > 7) {
	        	$.messager.alert('提示信息', '大写总额，不能大于7位数！');
	        	return;
	        }

			var num = parseInt($("#money01").val()) + parseInt($("#money02").val()) + parseInt($("#money03").val());
			$("#totalSmall").val(num);
			$("#totalBig").val(hs(num));

	        $.messager.progress({msg:'正在增加...',interval:3000,});
			$.ajax({
				type:"post",
				url:method,
				data:{
					paymentUnit:paymentUnit,
					date:date,
					collectionItem01:collectionItem01,
					money01:money01,
					collectionItem02:collectionItem02,
					money02:money02,
					collectionItem03:collectionItem03,
					money03:money03,
					totalSmall:totalSmall,
					totalBig:totalBig,
					remarks:remarks,
					payeeName:payeeName,
					id:id,
				},
				cache: false,
				dataType:'html',
				success:function(data){

					var data = eval('('+data+')');
					if( data.info == 'success' ){
						
						$.messager.progress('close');
						$('#action').dialog('close');
						$('#data').datagrid('reload');
					} else {
						
						$.messager.progress('close');
						$.messager.alert('提示信息', data.info);
					}
				}
			});

		}

		function doAdd(id, data){

			$("#action form").form('reset');
			$("#pic").show();
			$("#picChild").html(" ");
			$("#picImageChild").html(" ");

			// 日期方法
			var d = new Date();
			var str = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
			$(".data").val(str);

			var title = '新增';
			if(id > 0){
				var title = '修改';
				$("#paymentUnit").val(data.paymentUnit);
				$("#date").val(data.date);
				$("#collectionItem01").val(data.collectionItem01);
				$("#money01").val(data.money01);
				$("#collectionItem02").val(data.collectionItem02);
				$("#money02").val(data.money02);
				$("#collectionItem03").val(data.collectionItem03);
				$("#money03").val(data.money03);
				$("#totalSmall").val(data.totalSmall);
				$("#totalBig").val(data.totalBig);
				$("#remarks").val(data.remarks);
				$("#payeeName").val(data.payeeName);

				$("#id").val(data.id);

				var num = parseInt($("#money01").val()) + parseInt($("#money02").val()) + parseInt($("#money03").val());
				$("#totalSmall").val(num);
				$("#totalBig").val(hs(num));
			}
			
			$("#money01").change(function(){

				var num = parseInt($("#money01").val()) + parseInt($("#money02").val()) + parseInt($("#money03").val());
				$("#totalSmall").val(num);
				$("#totalBig").val(hs(num));
			});
			$("#money02").change(function(){
				
				var num = parseInt($("#money01").val()) + parseInt($("#money02").val()) + parseInt($("#money03").val());
				$("#totalSmall").val(num);
				$("#totalBig").val(hs(num));
			});
			$("#money03").change(function(){
				
				var num = parseInt($("#money01").val()) + parseInt($("#money02").val()) + parseInt($("#money03").val());
				$("#totalSmall").val(num);
				$("#totalBig").val(hs(num));
			});

			$('#action').show();
			$('#action').dialog('open').dialog({
				title:title,    
				width:"100%",    
				height:"100%",
				closed:false,    
				cache:false,    
				modal:true,
				closable:true, 
				left:0,
				top:0,
				buttons:[{
					text:'确定',
					width:'56',
					handler:function(){

						submit(id);
					}
				},{
					text:'取消',
					width:'56',
					handler:function(){

						$('#action').dialog('close');
					}
				}],
			});

		}

	</script>
