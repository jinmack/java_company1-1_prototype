package com.web.config;

import com.jfinal.config.Routes;
import com.web.remote.service.ImageCode;

public class ServiceRoutesConfig extends Routes {

	@Override
	public void config() {
		
		add("/i", ImageCode.class);
	}

}
