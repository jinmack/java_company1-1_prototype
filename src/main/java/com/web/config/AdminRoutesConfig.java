package com.web.config;

import com.jfinal.config.Routes;
import com.web.action.admin.AdminAction;
import com.web.action.admin.AdminAdminAuthorityAction;
import com.web.action.admin.AdminAdminUserAction;

public class AdminRoutesConfig extends Routes {

	@Override
	public void config() {
		
		add("/admin", AdminAction.class);
		add("/adminAdminUser", AdminAdminUserAction.class);
		add("/adminAdminAuthority", AdminAdminAuthorityAction.class);
	}

}
