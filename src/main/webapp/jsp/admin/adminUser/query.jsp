<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

	<div id="query" style="background:#fff;">

		<table>
			<tr>
				<td>员工名称：</td>
				<td>
					<input type="text" id="userNameQuery" placeholder="请输入员工名称">
				</td>
				<td>邮箱：</td>
				<td>
					<input type="text" id="emailQuery" placeholder="请输入邮箱">
				</td>
				<td>联系电话：</td>
				<td>
					<input type="text" id="phoneQuery" placeholder="请输入联系电话">
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
					<button class="btn btn-default btn-xs" href="javascript:void(0)" onclick="doQuery()">查询</button>
				</td>
				<td></td>
			</tr>
		</table>
	</div>

	<script type="text/javascript">

		function doQuery() {

			var queryObject = new Object();
			queryObject.userName = $("#userNameQuery").val();
			queryObject.email = $("#emailQuery").val();
			queryObject.phone = $("#phoneQuery").val();

			$("#data").datagrid('load',queryObject);	
		}

	</script>
