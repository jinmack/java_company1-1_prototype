<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<jsp:include page="/jsp/admin/common/head.jsp"></jsp:include>
<jsp:include page="/jsp/admin/bill/query.jsp"></jsp:include>

	<table id="data"></table>

	<script type="text/javascript">
		
		(function($){

			setTimeout("data()",100); 
		})(jQuery);

		// 数据查询
		function data(){

			$('#data').datagrid({    
		    	url:'/adminBill/billJsonSeleft',
				method:'post',
				fit:true,
				fitColumns:true,
				striped:true,
				border:false,
				pagination:true,
				rownumbers:false,
				toolbar:'#query',
				singleSelect:true,
				pageSize:30,//每页显示的记录条数，默认为10 
		        pageList:[10,20,30,40,50,500,1000],//可以设置每页记录条数的列表 
			    columns:[[
			    	 {field:'id',title:'id',width:20,align:'center'},	    	 
					 {field:'paymentUnit',title:'付款单位（人）',width:40,align:'center'},
					 {field:'payeeName',title:'收款人',width:40,align:'center'},
					 {field:'createTime',title:'创建时间',width:40,align:'center'},

					 {field:'operation',title:'操作',width:20,align : 'center', 
						 formatter:function(value,row,index){
						 	var id = row.id; 
						 	var fileAddress = row.fileAddress; 
						 	var str = "<button class='btn btn-default btn-xs' style='margin:2px;' data="+fileAddress+" onClick='download(this)'>导出</button>&nbsp;"
									+ "<button class='btn btn-default btn-xs' style='margin:2px;' onClick='saveData("+id+")'>编辑</button>&nbsp;"
								    + "<button class='btn btn-default btn-xs' style='margin:2px;' onClick='saveStatus("+id+","+-100+")'>删除</button>"
							return str;
						 }
					 }
			    ]],
				onLoadError:function(info){

				},
				onLoadSuccess:function(info){

					// setTimeout("doAdd()",10);
				},
				
			});  

			var p = $('#data').datagrid('getPager'); 
		    $(p).pagination({ 
		        beforePageText:'第',//页数文本框前显示的汉字 
		        afterPageText:'页 共 {pages} 页', 
		        displayMsg:'当前显示 {from} - {to} 条记录 共 {total} 条记录', 
				buttons:[{
					iconCls:'icon-add',
					handler:function(){
						doAdd();
					}
				},'-',{
					iconCls:'icon-help',
					handler:function(){
						alert('help');
					}
				}]
		    }); 
		} 

		// 状态更新
		function saveStatus(id, num){

			$.messager.confirm('确认','您确认想要执行吗？',function(r){    
				if(r){
			    	$.messager.progress({msg:'正在执行...',interval:3000,});
					$.ajax({
						type: "post",
						url: "/adminBill/billSaveStatus",
						data:{
							id:id,
							num:num,
						},
						cache: false,
						dataType:'html',
						success: function(data){
							var data = eval('('+data+')');
							if( data.info == 'success' ){
								
								$.messager.progress('close');
								$('#action').dialog('close');
								$('#data').datagrid('reload');   
							} else {
								
								$.messager.progress('close');
								$.messager.alert('提示信息', data.info);
							}
						}
					});
				}
			});
		}

		// 数据更新
		function saveData(id){

			$.messager.progress({msg:'正在查询...',interval:3000,});
			$.ajax({
				type:"post",
				url:"/adminBill/billIdSelect",
				data:{
					id:id,
				},
				cache: false,
				dataType:'html',
				success:function(data){

					var data = eval('('+data+')');
					if( data.info != 'success' ){
						
						$.messager.progress('close');
						$.messager.alert('提示信息', data.info);
					} else {

						$.messager.progress('close');
						doAdd(id, data.rows);
					}
				}
			});
		}

		// 文件下载
		function download(el){
			var fileAddress = $(el).attr('data');

	    	$.messager.progress({msg:'正在执行...',interval:3000,});
			$.ajax({
				type: "post",
				url: "/adminBill/download",
				data:{
					fileAddress:fileAddress,
				},
				cache: false,
				dataType:'html',
				async:false,
				success: function(data){

					try{
						var data = eval('('+data+')');
					} catch(e){

					}

					if (data.info != null) {

						$.messager.progress('close');
						$.messager.alert('提示信息', data.info);

					} else {

						$.messager.progress('close');
						window.open("/adminBill/download?fileAddress=" + fileAddress);
					}
				}
			});
		}

	</script>

<jsp:include page="/jsp/admin/bill/add.jsp"></jsp:include>
<jsp:include page="/jsp/admin/common/tail.jsp"></jsp:include>
