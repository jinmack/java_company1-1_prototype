
(function($){

	product = {

		//删除图片 - 单图
		removePic:function(el){

			$(el).parent().remove();
			$("#pic").show();
		},
		//上传图片并显示 - 单图
		upPic:function(el){

			var id = $(el).attr('id');
			$.messager.progress({msg:'正在增加...',interval:3000,});
			$.ajaxFileUpload({
				url:'/p/singlePicture',
				type:'post',
				secureuri:true,
				fileElementId:id,
				dataType:'text/html',
				success:function(data,status){

					var data = eval('('+data+')');
					$.messager.progress('close');
					if( data.info == 'success' ){

						$('#'+id+'Child').append("<div>"
												+"<img width='250' height='150' src='"+data.picAddress+"'/>"
												+"<button onclick='product.removePic(this)'>删除</button>"
												+"</div>");
						$("#"+id).hide();

					} else {
						
						$.messager.alert('提示信息', data.info);
					}
				}
			})
		},
	}

	authority = {

		// 权限列表
		// select标签组
		list:function(id){

			$("#authority").empty();
			$.get("/adminAdminAuthority/adminAuthorityList",
				function(data){
					if(data != null){
						var str = '<select>';
						str += '<option value="0">无</option>';
						for(var i = 0; i<data.rows.length; i++){
							if(data.rows[i].id == id){
								str += "<option value='"+data.rows[i].id+"' selected='selected' >"+data.rows[i].roleName+"</option>";
							}else{
								str += "<option value='"+data.rows[i].id+"'>"+data.rows[i].roleName+"</option>";
							}
						}
						str += '</select>';
						$("#authority").append(str);
					}
				}
			);
		},

	}

})(jQuery);
