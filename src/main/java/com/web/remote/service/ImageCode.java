package com.web.remote.service;

import com.jfinal.captcha.CaptchaRender;

public class ImageCode extends BaseService {
	
	protected static String captchaName = "captcha";
	
	@SuppressWarnings("static-access")
	public void code(){

		CaptchaRender img = new _CaptchaRender();
		img.setCaptchaName(captchaName);
		render(img);
	}
	
	
}
