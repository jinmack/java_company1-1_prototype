package com.web.action.admin;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.HashKit;
import com.jfinal.plugin.activerecord.Page;
import com.web.remote.dao.AdminAuthority;
import com.web.remote.dao.AdminUser;
import com.web.utils.EncryptstrUtils;

public class AdminAdminUserAction extends AdminBaseAction {

	public void index() {

		// renderText("/user/index");
		// System.out.println("index");
		// render("index.html");
		// renderNull();
		renderError(404);
	}
	
	// 员工页面显示
	public void adminUser(){
		
		renderJsp("/jsp/admin/adminUser/adminUser.jsp");
	}
	
	// 员工数据查询
	public void adminUserJsonSeleft(){
		
		int page = getParaToInt("page", 1);
		int rows = getParaToInt("rows", 10);
		String userName = getPara("userName", "");
		String email = getPara("email", "");
		String phone = getPara("phone", "");

		Page<AdminUser> adminUser = AdminUser.dao.paginate(page, rows, userName, email, phone);

		List<AdminAuthority> adminAuthority = AdminAuthority.dao.adminAuthorityList();

		for (int i = 0; i < adminUser.getList().size(); i++) {
			for (int j = 0; j < adminAuthority.size(); j++) {
				if (adminUser.getList().get(i).getAdminAuthorityId() == adminAuthority.get(j).getId()) {
					adminUser.getList().get(i).put("adminAuthority", adminAuthority.get(j));
				}
			}
		}

		setAttr("info", "success");
		setAttr("rows", adminUser.getList());
		setAttr("total", adminUser.getTotalRow());
		renderJson(new String[]{"info", "rows", "total"});
	}
	
	// 员工数据添加
	public void adminUserAdd(){

		String userName = getPara("userName", "");
		String password = getPara("password", "");
		String email = getPara("email", "");
		String phone = getPara("phone", "");
		Integer adminAuthorityId = getParaToInt("adminAuthorityId", 0);
		
		if (userName == "") {
			renderJson("info", "请输入员工名称！");
			return;
		}
		if (password == "") {
			renderJson("info", "请输入密码！");
			return;
		}
		if (email == "") {
			renderJson("info", "请输入邮箱！");
			return;
		}
		if (phone == "") {
			renderJson("info", "请输入电话！");
			return;
		}
		if (adminAuthorityId < 0) {
			renderJson("info", "请输入角色！");
			return;
		}

		AdminUser model = new AdminUser();
		model.setUserName(userName);
		model.setPassword(EncryptstrUtils.encrypt(password));
		model.setEmail(email);
		model.setPhone(phone);
		model.setAdminAuthorityId(adminAuthorityId);
		model.setStatus(0);
		model.setCreateTime(new Date());
		
		if (!AdminUser.dao.adminUserAdd(model)) {
			renderJson("info", "sql数据错误！");
			return;
		};

		setAttr("info", "success");
		renderJson(new String[]{"info"});
	}
	
	// 员工状态修改
	public void adminUserSaveStatus(){
		
		Integer id = getParaToInt("id", 0);
		Integer num = getParaToInt("num", 0);
		
		if (id <= 0) {
			renderJson("info", "数据错误！");
			return;
		}
		
		AdminUser model = new AdminUser();
		model.setId(id);
		model.setStatus(num);
		if (!AdminUser.dao.adminUserSaveData(model)) {
			renderJson("info", "sql数据错误！");
			return;
		};
		
		setAttr("info", "success");
		renderJson(new String[]{"info"});
	}
	
	// 员工id查询
	public void adminUserIdSelect(){
		
		int id = getParaToInt("id", 0);
		
		if (id <= 0) {
			renderJson("info", "数据错误！");
			return;
		}
		
		AdminUser adminUser = AdminUser.dao.adminUserIdSelect(id);

		setAttr("info", "success");
		setAttr("rows", adminUser);
		renderJson(new String[]{"info", "rows"});
	}
	
	// 员工数据修改
	public void adminUserSaveData(){

		String userName = getPara("userName", "");
		String password = getPara("password", "");
		String email = getPara("email", "");
		String phone = getPara("phone", "");
		Integer adminAuthorityId = getParaToInt("adminAuthorityId", 0);
		Integer id = getParaToInt("id", 0);
		
		if (userName == "") {
			renderJson("info", "请输入员工名称！");
			return;
		}
		if (password == "") {
			renderJson("info", "请输入密码！");
			return;
		}
		if (email == "") {
			renderJson("info", "请输入邮箱！");
			return;
		}
		if (phone == "") {
			renderJson("info", "请输入电话！");
			return;
		}
		if (adminAuthorityId < 0) {
			renderJson("info", "请输入角色！");
			return;
		}
		if (id <= 0) {
			renderJson("info", "数据错误！");
			return;
		}
		List<AdminUser> loginAdminUser = AdminUser.dao.loginAdminUser(phone);
		if (loginAdminUser.isEmpty()) {
			renderJson("info", "无效的邮箱/手机！");
			return;
		}

		if (loginAdminUser.get(0).getPassword().equals(password)) {
			// 输入密码相等，不做修改
			AdminUser model = new AdminUser();
			model.setId(id);
			model.setUserName(userName);
			model.setEmail(email);
			model.setPhone(phone);
			model.setAdminAuthorityId(adminAuthorityId);
			if (!AdminUser.dao.adminUserSaveData(model)) {
				renderJson("info", "sql数据错误！");
				return;
			};
		} else {
			// 输入密码不相等，做修改
			AdminUser model = new AdminUser();
			model.setId(id);
			model.setUserName(userName);
			model.setPassword(EncryptstrUtils.encrypt(password));
			model.setEmail(email);
			model.setPhone(phone);
			model.setAdminAuthorityId(adminAuthorityId);
			if (!AdminUser.dao.adminUserSaveData(model)) {
				renderJson("info", "sql数据错误！");
				return;
			};
		}

		setAttr("info", "success");
		renderJson(new String[]{"info"});
	}

	public static void main(String[] args) {
		
		System.out.println(HashKit.md5("123"));
		System.out.println(HashKit.sha256("123"));
		System.out.println(EncryptstrUtils.encrypt("123"));//加密   
		System.out.println(new EncryptstrUtils().decode("904014303"));
		
	}
	
}
